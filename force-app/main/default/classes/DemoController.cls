public with sharing class DemoController {
    /**
     * An empty constructor for the testing
     */
    public DemoController() {}

    /**
     * Get the version of the SFDX demo app
     */
    public String getAppVersion() {
    System.debug('Comment-1 From Release-1 branch');
        return '1.0.0';
    }
}
